@extends('layouts.cms_panel.auth')

@section('content')
    <div class="mai-wrapper mai-login">
        <div class="main-content container">
            <div class="splash-container row">
                <div class="col-md-6 user-message"><span class="splash-message text-right">Hello!<br> is good to<br> see you again</span><span class="alternative-message text-right">{{-- Don't have an account? <a href="{{ route('cms_panel.auth.register') }}">Sign Up</a>--}}</span></div>
                <div class="col-md-6 form-message"><span class="splash-description text-center mt-5 mb-5">Login to your account</span>
                    <form method="POST" action="{{ route('cms_panel.auth.login') }}">
                        {{ csrf_field() }}
                        @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group"><span class="input-group-addon"><i class="icon s7-user"></i></span>
                                <input id="email" type="email" class="form-control" placeholder="Email or login" name="email" value="{{ old('email') }}" autocomplete="off" required autofocus>

                            </div>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group"><span class="input-group-addon"><i class="icon s7-lock"></i></span>
                                <input id="password" type="password" placeholder="Password" name="password" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group login-submit">
                            <button data-dismiss="modal" type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                        </div>
                        <div class="form-group row login-tools">
                            <div class="col-sm-6 login-remember">
                                <label class="custom-control custom-checkbox mt-2">
                                    <input type="checkbox" name="remember" class="custom-control-input" {{ old('remember') ? 'checked' : '' }}><span class="custom-control-indicator"></span><span class="custom-control-description">Remember me</span>
                                </label>
                            </div>
                            <div class="col-sm-6 pt-2 text-sm-right login-forgot-password">{{-- <a href="{{ route('cms_panel.auth.password.request') }}">Forgot Password?</a> --}}</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection