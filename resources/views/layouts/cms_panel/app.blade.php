<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>

    <link rel="shortcut icon" href="/img/favicon.png">

    <link rel="stylesheet" type="text/css" href="/design/lib/stroke-7/v1.0.1/style.css"/>
    <link rel="stylesheet" type="text/css" href="/design/lib/perfect-scrollbar/v0.6.16/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" href="/design/lib/maisonnette/v1.2.1/css/app.min.css" type="text/css"/>
    @yield('head_plus')
</head>
<body>
<nav class="navbar navbar-expand navbar-dark mai-top-header">
    <div class="container"><a href="#" class="navbar-brand"></a>
        <!--Left Menu-->
        <ul class="nav navbar-nav mai-top-nav">
            <li class="nav-item"><a href="/cms-panel" class="nav-link">Home</a></li>

        </ul>
        <!--Icons Menu-->

        <!--User Menu-->
        <ul class="nav navbar-nav float-lg-right mai-user-nav">
            <li class="dropdown nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle nav-link"> <img src="/design/lib/maisonnette/v1.2.1/img/avatar.jpg"><span class="user-name">Super Admin</span><span class="angle-down s7-angle-down"></span></a>
                <div role="menu" class="dropdown-menu">


                    <a href="{{ route('cms_panel.auth.logout') }}" class="dropdown-item"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();"> <span class="icon s7-power"> </span> Logout</a>

                    <form id="logout-form" action="{{ route('cms_panel.auth.logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="mai-wrapper">
    <nav class="navbar navbar-expand-lg mai-sub-header">
        <div class="container">
            <!-- Mega Menu structure-->
            <nav class="navbar navbar-expand-md">
                <button type="button" data-toggle="collapse" data-target="#mai-navbar-collapse" aria-controls="#mai-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler hidden-md-up collapsed">
                    <div class="icon-bar"><span></span><span></span><span></span></div>
                </button>
                <div id="mai-navbar-collapse" class="navbar-collapse collapse mai-nav-tabs">
                    <ul class="nav navbar-nav">
                        <li class="nav-item parent open"><a href="#" role="button" aria-expanded="false" class="nav-link"><span class="icon s7-home"></span><span>Home</span></a>
                            <ul class="mai-nav-tabs-sub mai-sub-nav nav">
                                <li class="nav-item"><a href="/cms-panel" class="nav-link active"><span class="icon s7-monitor"></span><span class="name">Dashboard</span></a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>

        </div>
    </nav>
    <div class="main-content container">

        @yield('content')

    </div>
</div>
<script src="/design/lib/jquery/v3.2.1/jquery.min.js" type="text/javascript"></script>
<script src="/design/lib/perfect-scrollbar/v0.6.16/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="/design/lib/bootstrap/v4.0.0-beta.2/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="/design/lib/maisonnette/v1.2.1/js/app.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
    });
</script>
</body>
</html>