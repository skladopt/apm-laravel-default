<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('design/lib/stroke-7/v1.0.1/style.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('design/lib/perfect-scrollbar/v0.6.16/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('design/lib/font-awesome/v4.7.0/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('design/lib/maisonnette/v1.2.1/css/app.css') }}" type="text/css"/>
</head>
<body class="mai-splash-screen">
@yield('content')
<script src="{{ asset('design/lib/jquery/v3.2.1/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('design/lib/perfect-scrollbar/v0.6.16/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('design/lib/bootstrap/v4.0.0-beta.2/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('design/lib/maisonnette/v1.2.1/js/app.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        //initialize the javascript
        App.init();
    });

</script>
</body>
</html>