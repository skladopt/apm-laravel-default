<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'first_name' => 'Admin',
            'last_name' => 'Super',
            'email' => 'admin@site.com',
            'password' => bcrypt('123456'),
        ]);
        DB::table('roles')->insert([
            ['id' => 1, 'title' => 'Super Admin'],

        ]);
        DB::table('role_user')->insert([
            ['user_id' => '1', 'role_id' => '1']
        ]);
        Model::unguard();
    }
}